#ifndef TEST_OBJECTS_DETECTION_PRELEARNER_H
#define TEST_OBJECTS_DETECTION_PRELEARNER_H

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <opencv2/opencv.hpp>

#include "LibUtilities.h"
#include "Detector.h"

class PreLearner {
public:
    PreLearner(int argc, char *argv[], std::string dataset_path, std::string saving_folder);

    void createSamples(std::string dataset_type);

    static void get_imagename(std::string &image_name);

private:
    std::string dataset_path, saving_folder;

    boost::shared_ptr<VeryFastDetector> detector;

    boost::shared_ptr<std::ofstream> positive_stream, negative_stream;

    int compressed_size;

    void processPositiveSample(std::string dataset_type);

    void processNegativeSample(std::string dataset_type);

    void matchObjects(std::string dataset_type,
                      cv::Mat image,
                      std::string image_name,
                      std::vector<cv::Rect> real_objects,
                      std::vector<cv::Rect> detected_objects);

    void writeSample(std::string dataset_type, cv::Mat image, std::string image_name, cv::Rect detected, bool is_match_found);

    cv::Mat crop(cv::Mat image, cv::Rect detection);
};

#endif //TEST_OBJECTS_DETECTION_PRELEARNER_H