#ifndef TEST_OBJECTS_DETECTION_DETECTOR_H
#define TEST_OBJECTS_DETECTION_DETECTOR_H

#include "ObjectsDetectionApplication.hpp"
#include "LibUtilities.h"

class Detector {
public:
    virtual int init(int argc, char* argv[]) = 0;

    virtual std::vector<cv::Rect> compute(cv::Mat img) = 0;
};

class VeryFastDetector : public Detector {
public:
    VeryFastDetector();

    int init(int argc, char* argv[]);

    std::vector<cv::Rect> compute(cv::Mat img);

private:
    boost::shared_ptr<doppia::ObjectsDetectionApplication> app;
};

class PseudoDetector : public Detector {
public:
    PseudoDetector(std::string dataset_path, int test_number);

    int init(int argc, char* argv[]);

    std::vector<cv::Rect> compute(cv::Mat img);

    void checkStatisticsComputer(boost::shared_ptr<Estimates> estimates);

private:
    int test_number;

    boost::shared_ptr<Estimates> estimates;

    std::string dataset_path;

    std::vector<std::vector<cv::Rect> > real_objects_set;

    void removeSomeDetections(std::vector<cv::Rect> &detected_objects);

    void addThrashDetections(cv::Size size, int added_count, std::vector<cv::Rect> &detected_objects);
};

#endif //TEST_OBJECTS_DETECTION_DETECTOR_H