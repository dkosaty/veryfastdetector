#ifndef TEST_OBJECTS_DETECTION_LIBUTILITIES_H
#define TEST_OBJECTS_DETECTION_LIBUTILITIES_H

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <boost/regex.hpp>
#include <opencv2/opencv.hpp>

class INRIAParser {
public:
    INRIAParser(std::string dataset_type);

    std::vector<cv::Rect> parse(std::ifstream &input, std::string &path_to_image);

private:
    boost::shared_ptr<boost::regex> regex_shape, regex_path;
};

class Sample {
public:
    Sample(std::string dataset_path, std::string dataset_type);

    std::map<std::string, std::vector<cv::Rect> > getData();

    std::vector<std::string> getNegativeFilenames();

    void readPositive();

    void readNegative();

private:
    std::string dataset_path;

    std::string dataset_type;

    std::map<std::string, std::vector<cv::Rect> > data;

    std::vector<std::string> negative_filenames;

    boost::shared_ptr<INRIAParser> parser;

    void readAnnotationFile(std::string annotation);
};

class Estimates {
public:
    Estimates();

    int true_positive, true_negative, false_positive, false_negative;

    double frame_per_second;

    friend std::ostream &operator<<(std::ostream &output, const boost::shared_ptr<Estimates> estimates);
};

#endif //TEST_OBJECTS_DETECTION_LIBUTILITIES_H
