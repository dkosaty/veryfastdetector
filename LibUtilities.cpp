#include "LibUtilities.h"

/* The implementation of the InriaParser */

INRIAParser::INRIAParser(std::string dataset_type) {
    regex_shape = boost::shared_ptr<boost::regex>(new boost::regex("\\((\\d+), (\\d+)\\) - \\((\\d+), (\\d+)\\)"));
    regex_path = boost::shared_ptr<boost::regex>(new boost::regex(dataset_type + "pos/(\\w)*.png"));
}

std::vector<cv::Rect> INRIAParser::parse(std::ifstream &input, std::string &path_to_image) {
    std::vector<cv::Rect> real_objects;

    while (!input.eof()) {
        std::string line;
        boost::smatch match;
        getline(input, line);

        if (regex_search(line, match, *regex_shape)) {
            cv::Rect rect;

            std::istringstream(match[1]) >> rect.x;
            std::istringstream(match[2]) >> rect.y;

            std::istringstream(match[3]) >> rect.width; rect.width -= rect.x;
            std::istringstream(match[4]) >> rect.height; rect.height -= rect.y;

            real_objects.push_back(rect);
        }

        if (regex_search(line, match, *regex_path))
            path_to_image = match[0];
    }

    return real_objects;
}

/* The implementation of the Sample */

Sample::Sample(std::string dataset_path, std::string dataset_type) {
    this->dataset_path = dataset_path;

    this->dataset_type = dataset_type;

    parser = boost::shared_ptr<INRIAParser>(new INRIAParser(dataset_type));
}

std::map<std::string, std::vector<cv::Rect> > Sample::getData() { return data; }

std::vector<std::string> Sample::getNegativeFilenames() { return negative_filenames; }

void Sample::readPositive() {
    std::ifstream positive_stream((dataset_path + dataset_type + "annotations.lst").c_str());

    if (!positive_stream.is_open())
        throw std::ios::failure("Couldn't open 'annotation.lst' file");

    while (!positive_stream.eof()) {
        std::string annotation;
        std::getline(positive_stream, annotation);

        if (annotation.empty())
            break;

        readAnnotationFile(annotation);
    }

    positive_stream.close();
}

void Sample::readAnnotationFile(std::string annotation) {
    std::ifstream annotation_stream((dataset_path + annotation).c_str());

    if (!annotation_stream.is_open())
        throw std::ios::failure("Couldn't open the annotation file of the positive picture");

    std::string path_to_image;
    std::vector<cv::Rect> real_objects = parser->parse(annotation_stream, path_to_image);

    annotation_stream.close();

    data.insert(std::pair<std::string, std::vector<cv::Rect> >(path_to_image, real_objects));
}


void Sample::readNegative() {
    std::ifstream negative_stream((dataset_path + dataset_type + "neg.lst").c_str());

    if (!negative_stream.is_open())
        throw std::ios::failure("Couldn't open 'neg.lst'");

    while (!negative_stream.eof()) {
        std::string path_to_image;
        negative_stream >> path_to_image;

        if (path_to_image.empty())
            break;

        negative_filenames.push_back(path_to_image);
    }

    negative_stream.close();
}

/* The implementation of the Estimates */

Estimates::Estimates() {
    true_positive = 0;
    true_negative = 0;
    false_positive = 0;
    false_negative = 0;

    frame_per_second = 0.;
}

std::ostream &operator<<(std::ostream &output, const boost::shared_ptr<Estimates> estimates) {
    output <<
        "TP = " << estimates->true_positive << ", " <<
        "FP = " << estimates->false_positive << ", " <<
        "TN = " << estimates->true_negative << ", " <<
        "FN = " << estimates->false_negative << ", " <<
        "FPS = " << estimates->frame_per_second << std::endl;

    return output;
}