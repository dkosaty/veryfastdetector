#include <iostream>
#include <boost/filesystem.hpp>
#include "StatisticsComputer.h"
#include "PreLearner.h"

int counter = 0;

void computeStatistics(int argc, char *argv[], std::string dataset_path) {
//    int test_number = 0;
//    boost::shared_ptr<PseudoDetector> detector(new PseudoDetector(dataset_path, test_number));

    boost::shared_ptr<VeryFastDetector> detector(new VeryFastDetector());

    int ret = detector->init(argc, argv);

    boost::shared_ptr<StatisticsComputer> stat(new StatisticsComputer(detector, dataset_path, "Train/"));
//    boost::shared_ptr<StatisticsComputer> stat(new StatisticsComputer(detector, dataset_path, "Test/"));

    boost::shared_ptr<Estimates> estimates(new Estimates());
    boost::array<double, 3> results = stat->compute(estimates);
    double precision = results[0], recall = results[1], accuracy = results[2];

//    detector->checkStatisticsComputer(estimates);

    std::ofstream results_stream("train_results.txt");
//    std::ofstream results_stream("test_results.txt");

    if (!results_stream.is_open())
        throw std::ios::failure("Couldn't open '_results.txt'");

    results_stream <<
            estimates <<
            "precision = " << precision << "%" << std::endl <<
            "recall = " << recall << "%" << std::endl <<
            "accuracy = " << accuracy << "%";

    results_stream.close();
}

void detectObjects(int argc, char *argv[], std::string dataset_path, std::string dataset_type) {
    boost::shared_ptr<VeryFastDetector> detector(new VeryFastDetector());

    detector->init(argc, argv);

    std::ifstream stream((dataset_path + dataset_type + "pos.lst").c_str());
//    std::ifstream stream((dataset_path + "Test/pos.lst").c_str());

    if (!stream.is_open())
        throw std::ios::failure("Couldn't open 'pos.lst'");

    while (!stream.eof()) {
        std::string image_path;
        stream >> image_path;

        if (image_path.empty())
            break;

        cv::Mat image = cv::imread((dataset_path + image_path).c_str());

        std::vector<cv::Rect> detected_objects = detector->compute(image);

        for (std::vector<cv::Rect>::iterator rect = detected_objects.begin(); rect < detected_objects.end(); ++rect)
            cv::rectangle(image, *rect, cv::Scalar(0, 0, 255), 5);

//        cv::imshow(image_path, image);
//        cv::waitKey();
//        cv::destroyWindow(image_path);
        PreLearner::get_imagename(image_path);
        std::string image_name = image_path;
        cv::imwrite("inria-with-detections/" + dataset_type + image_name + ".png", image);
    }

    stream.close();
}

void processingImage(int argc, char *argv[], std::string dataset_path, std::string dataset_type, std::string image_name) {
    boost::shared_ptr<VeryFastDetector> detector(new VeryFastDetector());

    detector->init(argc, argv);

    cv::Mat image = cv::imread((dataset_path + dataset_type + image_name).c_str());

    if (image.empty())
        throw cv::Exception(0, "Couldn't open the picture", "processing_image", "main.cpp", 84);

    std::vector<cv::Rect> detected_objects = detector->compute(image);

    for (size_t i = 0; i < detected_objects.size(); ++i)
        cv::rectangle(image, detected_objects[i], cv::Scalar(0, 0, 255), 5);

    boost::shared_ptr<Sample> sample (new Sample(dataset_path, dataset_type));
    sample->readPositive();
    std::map<std::string, std::vector<cv::Rect> > data = sample->getData();

    std::map<std::string, std::vector<cv::Rect> >::iterator record;
    for(record = data.begin(); record != data.end(); ++record) {
        if (record->first == dataset_type + image_name) {
            std::vector<cv::Rect> real_objects = record->second;
            for (size_t i = 0; i < real_objects.size(); ++i)
                cv::rectangle(image, real_objects[i], cv::Scalar(255, 0, 0), 5);
            break;
        }
    }

    cv::imshow(image_name, image);
    cv::waitKey();
    cv::destroyWindow(image_name);
}

void createDirectories(std::string saving_folder, std::string dataset_type) {
    boost::filesystem::path subfolder(saving_folder + dataset_type);
    boost::filesystem::create_directory(subfolder);

    boost::filesystem::path positive_folder(boost::filesystem::path(subfolder)/boost::filesystem::path("pos"));
    boost::filesystem::create_directory(positive_folder);

    boost::filesystem::path negative_folder(boost::filesystem::path(subfolder)/boost::filesystem::path("neg"));
    boost::filesystem::create_directory(negative_folder);
}

void createVeryFastSamples(int argc, char *argv[], std::string dataset_path, std::string saving_folder) {
    PreLearner pre_learner(argc, argv, dataset_path, saving_folder);

//    boost::filesystem::path folder(saving_folder);
//    boost::filesystem::create_directory(folder);

//    createDirectories(saving_folder, "Train/");
//    createDirectories(saving_folder, "Test/");

    pre_learner.createSamples("Train/");
//    pre_learner.createSamples("Test/");
}

int main(int argc, char *argv[]) {
    std::string dataset_path = "/home/dmitry/INRIAPerson/";

    try {
//        processingImage(argc, argv, dataset_path, "Test/", "pos/crop001602.png");
//        processingImage(argc, argv, dataset_path, "Train/", "pos/crop001020.png");

//        detectObjects(argc, argv, dataset_path, "Train/");
//        detectObjects(argc, argv, dataset_path, "Test/");

//        computeStatistics(argc, argv, dataset_path);

        createVeryFastSamples(argc, argv, dataset_path, "/home/dmitry/subinria-after-veryfast-32x32/");
    }
    catch (std::exception &error) {
        std::cerr << "A std::exception was raised: " << error.what() << std::endl;
        return 1;
    }
    catch (std::ifstream::failure error) {
        std::cerr << "A std::ifstream::failure was raised: " << error.what() << std::endl;
        return 1;
    }
    catch(cv::Exception &error) {
        std::cerr << "A cv::Exception was raised: " << error.what() << std::endl;
        return 1;
    }
    catch (...) {
        std::cerr << "An unknown exception was raised." << std::endl;
        return 1;
    }
}