#include "PreLearner.h"

int positive_counter, negative_counter;

static int sample_size = 0;

PreLearner::PreLearner(int argc, char **argv, std::string dataset_path, std::string saving_folder) {
    this->dataset_path = dataset_path;

    this->saving_folder = saving_folder;

    detector = boost::shared_ptr<VeryFastDetector> (new VeryFastDetector());
    int ret = detector->init(argc, argv);

    positive_stream = boost::shared_ptr<std::ofstream> (new std::ofstream());
    negative_stream = boost::shared_ptr<std::ofstream> (new std::ofstream());

//    compressed_size = 128;
    compressed_size = 32;
}

void PreLearner::get_imagename(std::string &image_name) {
    boost::smatch match;
    if (regex_search(image_name, match, boost::regex("(\\w*)\\.")))
        image_name = match[1];
}

void PreLearner::createSamples(std::string dataset_type) {
    positive_stream->open((saving_folder + dataset_type + "pos.lst").c_str());
    negative_stream->open((saving_folder + dataset_type + "neg.lst").c_str());

    if (dataset_type == "Train/")
        sample_size = 576;
    else if (dataset_type == "Test/")
        sample_size = 288;

    this->processPositiveSample(dataset_type);
    this->processNegativeSample(dataset_type);

    negative_stream->close();
    positive_stream->close();
}

void PreLearner::processPositiveSample(std::string dataset_type) {
    boost::shared_ptr<Sample> sample(new Sample(dataset_path, dataset_type));
    sample->readPositive();
    std::map<std::string, std::vector<cv::Rect> > data = sample->getData();

    std::map<std::string, std::vector<cv::Rect> >::iterator record;
    for (record = data.begin(); record != data.end(); ++record) {
        cv::Mat image = cv::imread(dataset_path + record->first);

        if (image.empty())
            throw cv::Exception(0, "Couldn't open the positive picture", "get_detection_and_marking", "main.cpp", 27);

        std::string image_name = record->first;
        PreLearner::get_imagename(image_name);

        std::vector<cv::Rect> real_objects = record->second;

        std::vector<cv::Rect> detected_objects = detector->compute(image);

        positive_counter = 0;
        negative_counter = 0;

        this->matchObjects(dataset_type, image, image_name, real_objects, detected_objects);

        if (std::distance(data.begin(), record) == sample_size) {
//            std::cout << "\n\n\nTHE NUMBER\n\n\n" << std::distance(data.begin(), record) << std::endl;
            break;
        }
    }
}

void PreLearner::processNegativeSample(std::string dataset_type) {
    boost::shared_ptr<Sample> sample(new Sample(dataset_path, dataset_type));
    sample->readNegative();
    std::vector<std::string> negative_filenames = sample->getNegativeFilenames();

    std::vector<std::string>::iterator image_path;
    for (image_path = negative_filenames.begin(); image_path < negative_filenames.end(); ++image_path) {
        cv::Mat image = cv::imread(dataset_path + *image_path);

        if (image.empty())
            throw cv::Exception(1, "Couldn't open the negative picture", "compute", "StatisticsComputer.cpp", 74);

        std::string image_name = *image_path;
        PreLearner::get_imagename(image_name);

        std::vector<cv::Rect> detected_objects = detector->compute(image);

        positive_counter = 0;
        negative_counter = 0;

        this->matchObjects(dataset_type, image, image_name, std::vector<cv::Rect>(), detected_objects);

        if (std::distance(negative_filenames.begin(), image_path) == sample_size) {
//            std::cout << std::distance(negative_filenames.begin(), image_path) << std::endl;
            break;
        }
    }
}

void PreLearner::matchObjects(std::string dataset_type,
                              cv::Mat image,
                              std::string image_name,
                              std::vector<cv::Rect> real_objects,
                              std::vector<cv::Rect> detected_objects) {
    bool is_match_found;

    std::vector<cv::Rect>::iterator detected;
    for (detected = detected_objects.begin(); detected < detected_objects.end(); ++detected) {
        is_match_found = false;

        std::vector<cv::Rect>::iterator real;
        for (real = real_objects.begin(); real < real_objects.end(); ++real) {
            cv::Rect intersect = *real & *detected;

            cv::Rect max_rectangle = ((*real).area() > (*detected).area()) ? *real : *detected;

            if (intersect.area() > 0.5 * max_rectangle.area()) {
                is_match_found = true;
                break;
            }
        }

        this->writeSample(dataset_type, image, image_name, *detected, is_match_found);
    }
}

void PreLearner::writeSample(std::string dataset_type, cv::Mat image, std::string image_name, cv::Rect detected, bool is_match_found) {
    if (detected.x >= 0 && detected.size().width >= 0 && detected.x + detected.size().width <= image.cols &&
        detected.y >= 0 && detected.size().height >= 0 && detected.y + detected.size().height <= image.rows) {
        cv::Mat cropped_image = this->crop(image, detected);

        std::stringstream ss;

        if (is_match_found) {
            ss << ++positive_counter;

            image_name = dataset_type + "pos/" + image_name + '_' + ss.str() + ".png";

            *positive_stream << image_name << std::endl;

            cv::imwrite(saving_folder + image_name, cropped_image);
        }
        else {
            ss << ++negative_counter;

            image_name = dataset_type + "neg/" + image_name + '_' + ss.str() + ".png";

            *negative_stream << image_name << std::endl;

            cv::imwrite(saving_folder + image_name, cropped_image);
        }
    }
}

cv::Mat PreLearner::crop(cv::Mat image, cv::Rect detection) {
    cv::Mat sub_image = image(detection);

    cv::Mat resized_image = cv::Mat(cv::Size(compressed_size, compressed_size), CV_8UC3);

    cv::resize(sub_image, resized_image, resized_image.size());

    return resized_image;
}