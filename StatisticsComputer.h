#ifndef TEST_OBJECTS_DETECTION_STATISTICSCOMPUTER_H
#define TEST_OBJECTS_DETECTION_STATISTICSCOMPUTER_H

#include "LibUtilities.h"
#include "Detector.h"

extern int counter;

class StatisticsComputer {
public:
    StatisticsComputer(boost::shared_ptr<Detector> detector, std::string dataset_path, std::string dataset_type);

    boost::array<double, 3> compute(boost::shared_ptr<Estimates> estimates);

private:
    std::string dataset_path;

    boost::shared_ptr<Sample> sample;

    boost::shared_ptr<Detector> detector;

    void estimatePositive(boost::shared_ptr<Estimates> estimates);

    void estimateNegative(boost::shared_ptr<Estimates> estimates);

    boost::shared_ptr<Estimates> matchObjects(std::vector<cv::Rect> real_objects,
                                              std::vector<cv::Rect> detected_objects);

    void putMarking(std::pair<std::string, cv::Mat> record,
                    std::vector<cv::Rect> real_objects,
                    std::vector<cv::Rect> detected_objects);
};

#endif //TEST_OBJECTS_DETECTION_STATISTICSCOMPUTER_H
