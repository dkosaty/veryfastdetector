#include "StatisticsComputer.h"

static int sample_size = 0;

StatisticsComputer::StatisticsComputer(boost::shared_ptr<Detector> detector,
                                       std::string dataset_path,
                                       std::string dataset_type) {
    this->dataset_path = dataset_path;

    this->detector = detector;

    sample = boost::shared_ptr<Sample>(new Sample(dataset_path, dataset_type));

    if (dataset_type == "Train/")
        sample_size = 576;
    else if (dataset_type == "Test/")
        sample_size = 288;

    sample->readPositive();

    sample->readNegative();
}

boost::array<double, 3> StatisticsComputer::compute(boost::shared_ptr<Estimates> estimates) {
    estimatePositive(estimates);

    estimateNegative(estimates);

    int TP = estimates->true_positive,
            TN = estimates->true_negative,
            FP = estimates->false_positive,
            FN = estimates->false_negative;

    estimates->frame_per_second /= counter;

    double precision = TP / double(TP + FP) * 100,
            recall = TP / double(TP + FN) * 100,
            accuracy = (TP + TN) / double(TP + TN + FP + FN) * 100;

    return boost::array<double, 3> {precision, recall, accuracy};
}

void StatisticsComputer::estimatePositive(boost::shared_ptr<Estimates> estimates) {
    std::map<std::string, std::vector<cv::Rect> > data = sample->getData();

    std::map<std::string, std::vector<cv::Rect> >::iterator record;
    for(record = data.begin(); record != data.end(); ++record) {
        std::string path_to_image = record->first;

        cv::Mat img = cv::imread((dataset_path + path_to_image).c_str());

        if (img.empty())
            throw cv::Exception(0, "Couldn't open the positive picture", "compute", "StatisticsComputer.cpp", 45);

        std::vector<cv::Rect> real_objects = record->second;

        clock_t start_time = std::clock();
            std::vector<cv::Rect> detected_objects = detector->compute(img);
        double run_time = (std::clock() - start_time)/(double)CLOCKS_PER_SEC;
        estimates->frame_per_second += 1./run_time;
        ++counter;

        boost::shared_ptr<Estimates> current_estimates = matchObjects(real_objects, detected_objects);

        estimates->true_positive += current_estimates->true_positive;
        estimates->false_positive += current_estimates->false_positive;
        estimates->false_negative += current_estimates->false_negative;

//        std::cout << "TP = " << current_estimates->true_positive <<
//                ", FP = " << current_estimates->false_positive <<
//                ", FN = " << current_estimates->false_negative << std::endl;
//
//        putMarking(std::pair<std::string, cv::Mat>(path_to_image, img), real_objects, detected_objects);

        if (std::distance(data.begin(), record) == sample_size)
            break;
    }
}

void StatisticsComputer::estimateNegative(boost::shared_ptr<Estimates> estimates) {
    std::vector<std::string> negative_filenames = sample->getNegativeFilenames();

    std::vector<std::string>::iterator path_to_image;
    for(path_to_image = negative_filenames.begin(); path_to_image < negative_filenames.end(); ++path_to_image) {
        cv::Mat img = cv::imread(dataset_path + *path_to_image);

        if (img.empty())
            throw cv::Exception(1, "Couldn't open the negative picture", "compute", "StatisticsComputer.cpp", 74);

        clock_t start_time = std::clock();
            std::vector<cv::Rect> detected_objects = detector->compute(img);
        double run_time = (std::clock() - start_time)/(double)CLOCKS_PER_SEC;
        estimates->frame_per_second += 1./run_time;
        ++counter;

        boost::shared_ptr<Estimates> current_estimates(new Estimates());

        if (detected_objects.empty())
            ++current_estimates->true_negative;
        else
            current_estimates->false_positive += detected_objects.size();

        estimates->true_negative += current_estimates->true_negative;
        estimates->false_positive += current_estimates->false_positive;

//        std::cout << "TN = " << current_estimates->true_negative <<
//                ", FP = " << current_estimates->false_positive << std::endl;
//
//        putMarking(std::pair<std::string, cv::Mat>(*path_to_image, img), std::vector<cv::Rect>(), detected_objects);

        if (std::distance(negative_filenames.begin(), path_to_image) == sample_size)
            break;
    }
}

boost::shared_ptr<Estimates> StatisticsComputer::matchObjects(std::vector<cv::Rect> real_objects,
                                                              std::vector<cv::Rect> detected_objects) {
    boost::shared_ptr<Estimates> current_estimates(new Estimates());

    bool is_match_found;

    std::vector<cv::Rect>::iterator detected;
    for (detected = detected_objects.begin(); detected < detected_objects.end(); ++detected) {
        is_match_found = false;

        std::vector<cv::Rect>::iterator real;
        for (real = real_objects.begin(); real < real_objects.end(); ++real) {
            cv::Rect intersect = *real & *detected;

            cv::Rect max_rectangle = ((*real).area() > (*detected).area()) ? *real : *detected;

            if (intersect.area() > 0.5 * max_rectangle.area()) {
                is_match_found = true;
                break;
            }
        }

        is_match_found ? ++current_estimates->true_positive : ++current_estimates->false_positive;
    }

    if (!is_match_found && real_objects.size() >= current_estimates->true_positive)
        current_estimates->false_negative += real_objects.size() - current_estimates->true_positive;
    else if (real_objects.size() >= current_estimates->true_positive + current_estimates->false_positive)
        current_estimates->false_negative += real_objects.size() - (current_estimates->true_positive + current_estimates->false_positive);

    return current_estimates;
}

void StatisticsComputer::putMarking(std::pair<std::string, cv::Mat> record,
                                    std::vector<cv::Rect> real_objects,
                                    std::vector<cv::Rect> detected_objects) {
    std::string path_to_image = record.first;
    cv::Mat img = record.second;

    for (std::vector<cv::Rect>::iterator real = real_objects.begin(); real < real_objects.end(); ++real)
        cv::rectangle(img, *real, cv::Scalar(255, 0, 0));

    std::vector<cv::Rect>::iterator detected;
    for (detected = detected_objects.begin(); detected < detected_objects.end(); ++detected)
        cv::rectangle(img, *detected, cv::Scalar(0, 0, 255));

    cv::imshow(path_to_image, img);
    cv::waitKey();
    cv::destroyWindow(path_to_image);
}