#include "Detector.h"

extern int counter;

/* The implementation of the 'VeryFast' detector */

VeryFastDetector::VeryFastDetector() {
    app = boost::shared_ptr<doppia::ObjectsDetectionApplication>(new doppia::ObjectsDetectionApplication());
}

int VeryFastDetector::init(int argc, char* argv[]) {
    return app->init(argc, argv);
}

std::vector<cv::Rect> VeryFastDetector::compute(cv::Mat img) {
    return app->compute(img);
}

/* The implementation of the PseudoDetector */

PseudoDetector::PseudoDetector(std::string dataset_path, int test_number) {
    this->test_number = test_number;

    estimates = boost::shared_ptr<Estimates>(new Estimates());

    this->dataset_path = dataset_path;
}

int PseudoDetector::init(int argc, char* argv[]) {
    boost::shared_ptr<Sample> sample(new Sample(dataset_path, "Test/"));

    sample->readPositive();

    sample->readNegative();

    std::map<std::string, std::vector<cv::Rect> > data = sample->getData();

    std::map<std::string, std::vector<cv::Rect> >::iterator record;
    for (record = data.begin(); record != data.end(); ++record)
        real_objects_set.push_back(record->second);

    return 0;
}

std::vector<cv::Rect> PseudoDetector::compute(cv::Mat img) {
    srand(time((0)));

    std::vector<cv::Rect> detected_objects;
    if (counter <= real_objects_set.size())
        detected_objects = real_objects_set[counter];

    int added_count = 10;
    switch (test_number) {
        case 0:
            // removing some detections
            if (detected_objects.empty())
                ++(this->estimates)->true_negative;
            removeSomeDetections(detected_objects);
            (this->estimates)->true_positive += detected_objects.size();
            break;
        case 1:
            // adding detections with trash
            (this->estimates)->true_positive += detected_objects.size();
            addThrashDetections(img.size(), added_count, detected_objects);
            (this->estimates)->false_positive += added_count;
            break;
        case 2:
            // removing all detections
            if (detected_objects.empty())
                ++(this->estimates)->true_negative;
            (this->estimates)->false_negative += detected_objects.size();
            detected_objects = {};
            break;
        case 3:
            // nothing to do with the detections
            if (detected_objects.empty())
                ++(this->estimates)->true_negative;
            (this->estimates)->true_positive += detected_objects.size();
            break;
        case 4:
            // removing all detections, and adding detections with trash
            (this->estimates)->false_negative += detected_objects.size();
            detected_objects = {};
            addThrashDetections(img.size(), added_count, detected_objects);
            (this->estimates)->false_positive += detected_objects.size();
            break;
        case 5:
            // removing some detections, and adding detections with trash
            removeSomeDetections(detected_objects);
            (this->estimates)->true_positive += detected_objects.size();
            addThrashDetections(img.size(), added_count, detected_objects);
            (this->estimates)->false_positive += added_count;
            break;
        case 6:
            // nothing to do with the detections, and adding some detections
            (this->estimates)->true_positive += detected_objects.size();
            addThrashDetections(img.size(), added_count, detected_objects);
            (this->estimates)->false_positive += added_count;
            break;
        default:
            break;
    }

    return detected_objects;
}

void PseudoDetector::removeSomeDetections(std::vector<cv::Rect> &detected_objects) {
    if (counter%10 == 0) {
        (this->estimates)->false_negative += detected_objects.size();
        detected_objects = {};
    }
}

void PseudoDetector::addThrashDetections(cv::Size image_size, int added_count, std::vector<cv::Rect> &detected_objects) {
    for (int i = 0; i < added_count; ++i) {
        cv::Size rect_size(image_size.width / 32, image_size.height / 16);
        cv::Point point(rand()%(image_size.width - 2*rect_size.width), rand()%(image_size.height - 2*rect_size.height));

        cv::Rect rect(point, rect_size);

        detected_objects.push_back(rect);

//        std::cout << "x = " << rect.x << ", y = " << rect.y <<", width = " << rect.width << ", height = " << rect.height << std::endl;
    }
}

void PseudoDetector::checkStatisticsComputer(boost::shared_ptr<Estimates> estimates) {
    std::cout << "\nTrue positive of the StatisticsDetector: " << estimates->true_positive << std::endl;
    std::cout << "True positive of the PseudoDetector: " << (this->estimates)->true_positive << std::endl;

    std::cout << "\nTrue negative of the StatisticsDetector: " << estimates->true_negative << std::endl;
    std::cout << "True negative of the PseudoDetector: " << (this->estimates)->true_negative << std::endl;

    std::cout << "\nFalse positive of the StatisticsDetector: " << estimates->false_positive << std::endl;
    std::cout << "False positive of the PseudoDetector: " << (this->estimates)->false_positive << std::endl;

    std::cout << "\nFalse negative of the StatisticsDetector: " << estimates->false_negative << std::endl;
    std::cout << "False negative of the PseudoDetector: " << (this->estimates)->false_negative << std::endl;
}